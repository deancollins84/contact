@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h1>Messages</h1>
                </div> 
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Message</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($messages as $message)
                            <tr scope="row">
                                <td ><strong>{{ $message->getName() }}</strong></td>
                                <td>{{ $message->getEmail() }}</td>
                                <td>{{ $message->getBody() }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer text-muted">
                    <a href="/" class="btn btn-danger">New message</a>
                    paginate links would follow ...
                </div>
            </div>
        </div>
    </div>
</div>
@endsection