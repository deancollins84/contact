@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {!! Form::open(['route' => 'contact.submit']) !!}
                {{ csrf_field() }}
                <div class="card">
                    <div class="card-header">
                        <h1>Contact</h1>
                        @if(($errors->any()))
                        <div class="alert alert-danger">
                            <h4>Please check the following ...</h4> 
                            @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                            @endforeach
                        </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', null, ['placeholder'=>'Enter name ...', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email') !!}                                   
                            {!! Form::text('email', null, ['placeholder'=>'Enter email ...', 'class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('message', 'Message') !!}
                            {!! Form::textarea('message', null, ['placeholder'=>'Enter message ...', 'class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        {!! Form::submit('Submit') !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection