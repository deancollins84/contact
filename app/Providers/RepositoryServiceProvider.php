<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\Interfaces\MessageRepository as MessageRepositoryInterface;
use App\Repositories\Doctrine\MessageRepository;

class RepositoryServiceProvider extends ServiceProvider {

    public function register() {
        
        $this->app->bind(MessageRepositoryInterface::class, MessageRepository::class);
    }

}
