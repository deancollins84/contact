<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\Interfaces\MessageService as MessageServiceInterface;
use App\Services\MessageService;

class ServiceServiceProvider extends ServiceProvider {

    public function register() {
        
        $this->app->bind(MessageServiceInterface::class, MessageService::class);
    }

}
