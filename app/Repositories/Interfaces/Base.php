<?php

namespace App\Repositories\Interfaces;

interface Base {
    
    public function repository();

    public function findAll();
    
    public function paginateAll(int $perPage = 15, int $page = 1);
    
}