<?php

namespace App\Repositories\Doctrine;

use App\Entities\Message;
use App\Repositories\Interfaces\MessageRepository as MessageRepositoryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use LaravelDoctrine\ORM\Pagination\PaginatesFromParams;

class MessageRepository implements MessageRepositoryInterface {

    use PaginatesFromParams;

    protected $_doctrine;

    public function __construct(ManagerRegistry $doctrine) {
        $this->_doctrine = $doctrine;
    }

    /**
     * Entity repository
     */
    public function repository() {
        return $this->_doctrine->getRepository(Message::class);
    }

    /**
     * Return all messages.
     */
    public function findAll() {
        return $this->repository()->findAll();
    }

    /**
     * Creates a new QueryBuilder instance that is prepopulated for this message entity.
     * @param type $alias
     * @param type $indexBy
     */
    public function createQueryBuilder($alias, $indexBy = null) {
        return $this->repository()->createQueryBuilder($alias, $indexBy);
    }

}