<?php

namespace App\Services\Interfaces;

interface MessageService {

    public function createAndStoreMessage(string $name, string $email, string $messageBody) : bool;
}