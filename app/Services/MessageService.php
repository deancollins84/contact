<?php

namespace App\Services;

use App\Services\Interfaces\MessageService as MessageServiceInterface;
use App\Entities\Message;
use Doctrine\ORM\EntityManager;
use Exception;

class MessageService implements MessageServiceInterface {

    protected $entityManager;

    public function __construct(EntityManager $doctrine) {
        $this->entityManager = $doctrine;
    }

    /**
     * Create and persist message.
     * @param string $name
     * @param string $email
     * @param string $messageBody
     * @return bool
     */
    public function createAndStoreMessage(string $name, string $email, string $messageBody) : bool {
        try {
            $message = new Message($name, $email, $messageBody);
            $this->entityManager->persist($message);
            $this->entityManager->flush();
            return true;
        }
        catch (Exception $exception){
            return false;
        }
    }

}
