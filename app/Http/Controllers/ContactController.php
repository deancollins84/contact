<?php

namespace App\Http\Controllers;

use App\Http\Requests\Message as MessageRequest;
use App\Repositories\Interfaces\MessageRepository;
use App\Services\Interfaces\MessageService;
use App\Entities\Message;
use Doctrine\ORM\EntityManager;
use Exception;

class ContactController extends Controller {

    /**
     * Index of messages.
     * @param MessageRepository $messageRepository
     * @return view
     */
    public function index(MessageRepository $messageRepository) {
        return view('contact.index')->with('messages', $messageRepository->paginateAll());
    }

    /**
     * Create message form.
     * @return view
     */
    public function create() {
        return view('contact.form');
    }

    /**
     * Submission of message.
     * @param MessageRequest $request
     * @param MessageService $messageService
     * @return redirect
     */
    public function store(MessageRequest $request, MessageService $messageService) {
        try {
            $messageService->createAndStoreMessage($request->input('name'), $request->input('email'), $request->input('message'));
            return redirect('contact/index', 201);
        } catch (Exception $exception){
           return back()->withException($exception);
        }
    }

}