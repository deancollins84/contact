<?php

namespace App\Entities\Interfaces;

interface Message {

    public function getName(): string;

    public function getEmail(): string;

    public function getBody(): string;
}