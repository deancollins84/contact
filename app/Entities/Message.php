<?php

namespace App\Entities;

use App\Entities\Interfaces\Message as MessageInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Basic message entity.
 * @ORM\Entity
 */
class Message implements MessageInterface {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=150)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=200)
     */
    protected $body;

    public function __construct(string $name, string $email, string $body) {
        $this->name = $name;
        $this->email = $email;
        $this->body = $body;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function getBody(): string {
        return $this->body;
    }

}