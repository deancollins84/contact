<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContactController@create')->name('contact.form');
Route::get('/contact/index', 'ContactController@index')->name('contact.index');
Route::post('/contact/submit', 'ContactController@store')->name('contact.submit');