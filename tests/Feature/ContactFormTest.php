<?php

namespace Tests\Feature;

use Tests\TestCase;

class ContactFormTest extends TestCase
{
    /**
     * Contact view form.
     * @return void
     */
    public function testViewForm()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
    
    /**
     * Successful form post passing validation.
     * @return void
     */
    public function testSuccessfulPostForm(){
        $response = $this->post('/contact/submit', ['name' => 'Sally', 'email' => 'sally@email.com', 'message' => 'Lorem ipsum dolor sit amet.']);
        $response->assertStatus(201);
    }
    
    /**
     * Validation errors on form submit.
     * 302 for redirect with validation errors.
     * @return void
     */
    public function testUnsuccessfulPostForm(){
        $response = $this->post('/contact/submit', ['email' => 'sally@email.com', 'message' => 'Lorem ipsum dolor sit amet.']);
        $response->assertStatus(302);
    }
    
    /**
     * Contact entries index.
     */
    public function testViewAllEntries(){
        $response = $this->get('/contact/index');
        $response->assertStatus(200);
    }
}
