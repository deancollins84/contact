<?php

namespace Tests\Unit\Entities;

use Tests\TestCase;
use App\Entities\Message;

class MessageTest extends TestCase {

    protected $message;

    public function setUp() {
        parent::setUp();
        $this->message = new Message('Dean', 'dean@email.com', 'Hello world!');
    }

    /**
     * Message name is string.
     * @return void
     */
    public function testNameIsString() {
        $this->assertInternalType('string', $this->message->getName());
    }

    /**
     * Message email is string.
     * @return void
     */
    public function testEmailIsString() {
        $this->assertInternalType('string', $this->message->getEmail());
    }

    /**
     * Message email is valid.
     * @return void
     */
    public function testEmailIsValid() {
        $this->assertRegExp('/^.+\@\S+\.\S+$/', $this->message->getEmail());
    }

    /**
     * Message body is string.
     * @return void
     */
    public function testBodyIsString() {
        $this->assertInternalType('string', $this->message->getBody());
    }

}
