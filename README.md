# Contact

2 page Laravel app, contact form and index view of messages.

## To possibly expand upon
Area to explore further in a refactor would maybe the factory method to be bought in, incase messages became more complex i.e. levels of enquires, types, additional fields, etc. 

## Installing

Composer install to include requirements including Doctrine 2 and Laravel Collective HTML packages.

## Minimum .env values

* APP_KEY= {{ APP KEY }}
* DB_CONNECTION=sqlite

## Running the tests

vendor/bin/phpunit for both feature and unit tests.

## Database

Using sqlite (seperate database for testing)

## Built With

* [Laravel] - The web framework used
* [Doctrine 2] - ORM

## Authors

* **Dean Collins **

